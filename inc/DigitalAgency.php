<?php
/**
 * Created by PhpStorm.
 * User: vivek
 * Date: 7/27/2018
 * Time: 2:02 PM
 */
require_once (dirname(__FILE__) . '/createConnection.php');
class DigitalAgency extends createConnection
{
    protected $name;
    protected $persons;

    public function __construct($name)
    {
        $this->persons = [];
        $this->name = $name;
    }

    public function setPerson(Person $person)
    {
        try {
            if ($this->validate($person)) {
                array_push($this->persons, $person);
            } else {
                $error = 'Invalid parameters';
                throw new Exception($error);
            }
        } catch (Exception $e) {
            print_r($e);
        }
    }


    public function getPerson()
    {
        return $this->persons;
    }

    public function saveAgency()
    {
        parent::__construct();

        foreach ($this->persons as $person){

            $sql = "INSERT INTO `details`.`details_agency` (`id`, `work`, `cname`, `city`) VALUES (Null, '".$this->name."', '" . $person->getName() . "', '" . $person->getCity() . "')";

            if (mysqli_query($this->dbCon, $sql)) {
                echo "inserted";
            } else {
                echo "not inserted";
            }
        }

    }

    protected function validate($person)
    {
        if (is_object($person)) {
            if (property_exists($person, 'name') && property_exists($person, 'city')) {
                return true;
            }

        }
        return false;
    }
}
$db = new createConnection();