<?php
require_once (dirname(__FILE__) . '/DigitalAgency.php');

class DigitalAgencyCustom extends DigitalAgency
{
    protected function validate($person)
    {
        $validName = false;
        $validCity = false;
        if (is_object($person)) {
            if (property_exists($person, 'name')) {
                if(strlen($person->getName())> 3)
                {
                    $validName=true;
                }
            }
            if (property_exists($person, 'city')) {
                if(strlen($person->getCity())>4)
                {
                    $validCity=true;
                }
            }

        }
        return ($validCity && $validName) ? true:false;
    }
}