<?php
/**
 * Created by PhpStorm.
 * User: vivek
 * Date: 7/27/2018
 * Time: 3:11 PM
 */

class Person
{
private $name;
private $city;

    /**
     * @return mixed
     */
    /**
     * @return mixed
     */
    public function setName($name)
    {
         $this->name=$name;
    }
    public function setCity($city)
    {
        $this->city=$city;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getCity()
    {
        return $this->city;
    }

    public function getPerson()
    {
        return [
        'name'=> $this->name,
        'city' =>$this->city,
    ];
    }
}