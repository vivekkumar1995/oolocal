<?php
/**
 * Created by PhpStorm.
 * User: vivek
 * Date: 7/28/2018
 * Time: 12:23 AM
 */

class createConnection
{
    public $server = 'localhost';
    public $user   = 'root';
    public $passwd = 'password';
    public $db     = 'details';
    public $dbCon;

    function __construct(){
        $this->dbCon = mysqli_connect($this->server, $this->user, $this->passwd, $this->db);
    }
}
